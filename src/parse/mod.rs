
use itertools::Itertools;

use nom::branch::alt;
use nom::bytes::complete::{tag, take_till1, take_until};
use nom::character::complete::{line_ending, space0, space1};
use nom::combinator::{map, opt};
use nom::multi::many0;
use nom::sequence::{delimited, pair, preceded, terminated, tuple};
use nom::IResult;
use std::iter;
mod value;
use value::value;

mod attributes;
use attributes::parse_attributes;

mod indentation;
use indentation::{parse_indentation, IndentationInfo};

use crate::structures::*;

fn single_to_vec<T>(e: T) -> Vec<T> {
    vec![e]
}

fn empty_line0(s: &str) -> IResult<&str, Vec<&str>> {
    many0(preceded(space0, line_ending))(s)
}

fn take_line(s: &str) -> IResult<&str, &str> {
    terminated(take_until("\n"), tag("\n"))(s)
}

fn tag_name(s: &str) -> IResult<&str, &str> {
    // From https://html.spec.whatwg.org/multipage/syntax.html#syntax-tag-name
    take_till1(|c: char| !c.is_alphanumeric() && c != '-')(s)
}


fn parse_text(info: IndentationInfo) -> impl Fn(&str) -> IResult<&str, Node> {
    move |s| {
        let (s, _) = parse_indentation(info)(s)?;

        let (s, text) = take_line(s)?;

        Ok((s, Node::Text(text.to_string())))
    }
}

fn normalize_text(elements: Vec<Node>) -> Vec<Node> {
    elements
        .into_iter()
        .coalesce(|a, b| {
            if let (Node::Text(a), Node::Text(b)) = (&a, &b) {
                Ok(Node::Text(format!("{} {}", a.trim(), b.trim())))
            } else {
                Err((a, b))
            }
        })
        .collect()
}

fn parse_node<'a>(info: IndentationInfo) -> impl Fn(&'a str) -> IResult<&'a str, Node> {
    move |s| {
        let (s, _) = parse_indentation(info)(s)?;

        alt((
            // Normal tag
            map(
                tuple((
                    tag_name,
                    opt(delimited(tag("("), parse_attributes, tag(")"))),
                    alt((
                        preceded(
                            pair(tag(":"), space1),
                            map(parse_node(info.same_line()), single_to_vec),
                        ),
                        preceded(tag(" "), map(parse_text(info.same_line()), single_to_vec)),
                        preceded(
                            pair(tag("."), line_ending),
                            map(
                                many0(preceded(empty_line0, parse_text(info.indented()))),
                                |v| {
                                    v.into_iter()
                                        .interleave_shortest(iter::repeat(Node::Text(
                                            "\n".to_string(),
                                        )))
                                        .collect()
                                },
                            ),
                        ),
                        preceded(
                            line_ending,
                            map(
                                many0(preceded(empty_line0, parse_node(info.indented()))),
                                normalize_text,
                            ),
                        ),
                    )),
                )),
                |(tag_name, attributes, children)| {
                    // Node::Element(Element {
                    //     tag_name: tag_name.to_string(),
                    //     attributes: attributes.unwrap_or_default(),
                    //     children,
                    // })

                    Node::Element(Element::builder(tag_name).attributes(attributes.unwrap_or_default()).children(children).unwrap())
                },
            ),
            // Pipe pseudo-element
            preceded(
                tag("|"),
                alt((
                    preceded(opt(tag(" ")), parse_text(info.same_line())),
                    value(line_ending, Node::text("\n")),
                )),
            ),

            // Comments
            preceded(tag("//-"), value(take_line, Node::UnbufferedNode)),
            preceded(tag("//"), map(take_line, Node::comment)),
        ))(s)

    }
}

pub fn parse_file(s: &str) -> IResult<&str, Vec<Node>> {
    let indentation_kind =
        indentation::IndentationKind::detect(s).expect("Unable to detect indentation");

    many0(preceded(
        many0(line_ending),
        parse_node(IndentationInfo::new(indentation_kind)),
    ))(s)
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn comments() {
        assert_eq!(
            parse_file("// Buffered\n"),
            Ok(("", vec![Node::comment(" Buffered")]))
        );
        assert_eq!(
            parse_file("//- Unbuffered\n"),
            Ok(("", vec![Node::UnbufferedNode]))
        );
    }

    #[test]
    fn pipe_pseudoelement() {
        assert_eq!(
            parse_file("| Some text\n"),
            Ok(("", vec![Node::text("Some text")]))
        );
    }

    #[test]
    fn indentation() {
        assert_eq!(
            parse_file("a\n\tb\n"),
            Ok((
                "",
                vec![Node::Element(
                    Element::builder("a")
                        .child(Node::Element(Element::new("b")))
                        .unwrap()
                )]
            ))
        )
    }
}