use nom::multi::count;
use nom::bytes::complete::tag;
use nom::IResult;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum IndentationKind {
    Tabs,
    Spaces(usize),
}

impl IndentationKind {
    pub fn detect(s: &str) -> Option<IndentationKind> {
        s.lines().find_map(|line| {
            let chars = line.chars().take_while(|&c| c == '\t' || c == ' ').collect::<Vec<_>>();

            match chars.get(0) {
                Some('\t') => Some(IndentationKind::Tabs),
                Some(' ') => Some(IndentationKind::Spaces(chars.len())),
                Some(_) | None => None,
            }
        })
    }
}

#[derive(Clone, Copy, PartialEq)]
pub struct IndentationInfo {
    kind: IndentationKind,
    level: usize,
    same_line: bool
}

impl IndentationInfo {
    pub fn new(kind: IndentationKind) -> IndentationInfo {
        IndentationInfo {
            kind,
            level: 0,
            same_line: false
        }
    }

    pub fn indented(&self) -> IndentationInfo {
        IndentationInfo {
            kind: self.kind,
            level: self.level + 1,
            same_line: false
        }
    }

    pub fn same_line(&self) -> IndentationInfo {
        IndentationInfo {
            kind: self.kind,
            level: self.level,
            same_line: true,
        }
    }
}

pub fn parse_indentation(info: IndentationInfo) -> impl Fn(&str) -> IResult<&str, ()> {
    let indentation = match info.kind {
        IndentationKind::Tabs => "\t".to_string(),
        IndentationKind::Spaces(n) => " ".repeat(n)
    };

    move |s| {
        if !info.same_line {
            count(tag(indentation.as_str()), info.level)(s).map(|(s, _)| (s, ()))
        } else {
            Ok((s, ()))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn detect() {
        assert_eq!(IndentationKind::detect("html\n    4 spaces\n"), Some(IndentationKind::Spaces(4)));
        assert_eq!(IndentationKind::detect("html\n\ttabs\n"), Some(IndentationKind::Tabs));
        assert_eq!(IndentationKind::detect("html\nnot detectable\n"), None);
    }
}