use nom::error::ParseError;
use nom::IResult;
use nom::combinator::map;

pub fn value<I, O1, O2: Clone, E: ParseError<I>, F>(
    first: F,
    second: O2,
) -> impl Fn(I) -> IResult<I, O2, E>
where
    F: Fn(I) -> IResult<I, O1, E>,
{
    map(first, move |_| second.clone())
}