extern crate nom;
extern crate itertools;
extern crate xml;

mod structures;
mod parse;
use parse::parse_file;

mod write;
use write::write_tree;

const TEST_STRING: &str = r#"
html
    head
        title
            32

    body
        h1(attr)
            | text in the head with another element
            span(style="color:red;") text after element
            | normalize-
            | able text
        f(attr) this is text
    
        g: h.
            i
        div(tricky='attributes"')
            | multiple pipe
            | elements

        script.
            console.log("this is a script element")
            console.log("with proper whitespace")

        // Html comments

        //- an unbuffered comment

        dash-test(data-attribute="value")

// Html Comments  2
//- second unbuffered comment


"#;


fn main() {
    let parsed_file = parse_file(TEST_STRING).unwrap();

    println!("Leftovers: {:?}", parsed_file.0);

    println!("{}", write_tree(&parsed_file.1).unwrap());
}
